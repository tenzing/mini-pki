# Simple PKI

This is a very simple PKI based on
[OpenSSL](https://www.openssl.org/). It handles the most basic CA life
cycle tasks and is preconfigured with (hopefully) sane defaults. It
uses [git](https://git-scm.com/) transparently to keep a history of
changes and to help recovering from mistakes.

Please note that this PKI does not handle the security of the CA
data. It is recommended to store the CA data on an encrypted loopback
and preferably on an air-gapped computer.

## Example

The example below creates one self-signed root CA, one intermediate
TLS-signing CA, and finally a TLS server certificate.

### Root CA

#### Initialise the CA directory
	$ ./setup-ca /safe/root-ca root
	Initialized empty Git repository in /safe/root-ca/.git/
	[master (root-commit) ac8cd12] Initial commit
	 6 files changed, 482 insertions(+)
	 create mode 100644 db/crl.serial
	 create mode 100644 db/crt.serial
	 create mode 100644 db/index
	 create mode 100644 db/index.attr
	 create mode 100644 openssl.conf
	 create mode 100755 pki

#### Configure the CA
	$ cd /safe/root-ca
	$ vi openssl.conf

#### Generate the CA private key
	$ ./pki genkey 8192
	
	 -[*]- Generating private key: ca/ca.key
	
	Generating RSA private key, 8192 bit long modulus
	.......................................................................................................................++
	...........................................................++
	e is 65537 (0x10001)
	Enter pass phrase for ca/ca.key:
	Verifying - Enter pass phrase for ca/ca.key:
	[master 535883b] Add private key: ca/ca.key
	 2 files changed, 103 insertions(+)
	 create mode 100644 ca/ca.key

#### Generate the CA certificate signing request
	$ ./pki gencsr
	
	 -[*]- Generating certificate signing request: ca/ca.csr
	
	Enter pass phrase for ca/ca.key:
	[master 99dc09e] Add certificate signing request: ca/ca.csr
	 2 files changed, 53 insertions(+), 1 deletion(-)
	 create mode 100644 ca/ca.csr

#### Generate the self-signed root certificate
	$ ./pki selfsign `expr 365 \* 30`
	
	 -[*]- Generating a self-signed certificate: ca/ca.crt
	
	Using configuration from openssl.conf
	Enter pass phrase for ca/ca.key:
	Check that the request matches the signature
	Signature ok
	Certificate Details:
	        Serial Number: 0 (0x0)
	        Validity
	            Not Before: Aug 21 11:16:15 2016 GMT
	            Not After : Aug 14 11:16:15 2046 GMT
	        Subject:
	            organizationName          = Example
	            organizationalUnitName    = Example PKI
	            commonName                = Example Root CA
	        X509v3 extensions:
	            X509v3 Basic Constraints: critical
	                CA:TRUE
	            X509v3 Key Usage: critical
	                Certificate Sign, CRL Sign
	            X509v3 Subject Key Identifier: 
	                F0:D4:E1:24:AE:F7:42:5A:26:E7:09:71:DF:3B:CA:CE:97:61:63:CD
	            X509v3 CRL Distribution Points: 
	
	                Full Name:
	                  URI:https://pki.example.com/root.crl
	
	            Authority Information Access: 
	                CA Issuers - URI:https://pki.example.com/root.html
	
	Certificate is to be certified until Aug 14 11:16:15 2046 GMT (10950 days)
	
	Write out database with 1 new entries
	Data Base Updated
	[master 36a29f6] Add self-signed certificate: ca/ca.crt
	 8 files changed, 426 insertions(+), 1 deletion(-)
	 create mode 100644 ca/ca.crt
	 create mode 100644 certs/00.pem
	 create mode 100644 db/crt.serial.old
	 create mode 100644 db/index.attr.old
	 create mode 100644 db/index.old

#### Generate the CRL
	$ ./pki gencrl
	
	 -[*]- Generating certificate revokation list: ca/ca.crl
	
	Using configuration from openssl.conf
	Enter pass phrase for ca/ca.key:
	[master 13bcd54] Update certificate revokation list: ca/ca.crl
	 3 files changed, 31 insertions(+), 1 deletion(-)
	 create mode 100644 ca/ca.crl
	 create mode 100644 db/crl.serial.old
	
### Intermediate TLS-signing CA

#### Initialise the CA directory
	$ ./setup-ca /safe/server-ca server
	./setup-ca /tmp/pki/server-ca server
	Initialized empty Git repository in /tmp/pki/server-ca/.git/
	[master (root-commit) f0d14eb] Initial commit
	 6 files changed, 483 insertions(+)
	 create mode 100644 db/crl.serial
	 create mode 100644 db/crt.serial
	 create mode 100644 db/index
	 create mode 100644 db/index.attr
	 create mode 100644 openssl.conf
	 create mode 100755 pki
	
#### Configure the CA
	$ cd /safe/server-ca
	$ vi openssl.conf

#### Generate the CA private key
	$ ./pki genkey 4096
	
	 -[*]- Generating private key: ca/ca.key
	
	Generating RSA private key, 4096 bit long modulus
	.........................................++
	..........................++
	e is 65537 (0x10001)
	Enter pass phrase for ca/ca.key:
	Verifying - Enter pass phrase for ca/ca.key:
	[master 00503c6] Add private key: ca/ca.key
	 1 file changed, 54 insertions(+)
	 create mode 100644 ca/ca.key

#### Generate the CA certificate signing request
	$ ./pki gencsr
	
	 -[*]- Generating certificate signing request: ca/ca.csr
	
	Enter pass phrase for ca/ca.key:
	[master 3646d67] Add certificate signing request: ca/ca.csr
	 1 file changed, 31 insertions(+)
	 create mode 100644 ca/ca.csr

#### Sign the CSR using the root CA
	$ cd /safe/root-ca
	$ ./pki sign /safe/server-ca/ca/ca.csr /safe/server-ca/ca/ca.crt
	
	 -[*]- Signing certificate request: ../server-ca/ca/ca.csr
	
	Using configuration from openssl.conf
	Enter pass phrase for ca/ca.key:
	Check that the request matches the signature
	Signature ok
	Certificate Details:
	        Serial Number: 1 (0x1)
	        Validity
	            Not Before: Aug 21 11:27:24 2016 GMT
	            Not After : Aug 21 11:27:24 2026 GMT
	        Subject:
	            organizationName          = Example
	            organizationalUnitName    = Example PKI
	            commonName                = Example Server CA
	        X509v3 extensions:
	            X509v3 Authority Key Identifier: 
	                keyid:F0:D4:E1:24:AE:F7:42:5A:26:E7:09:71:DF:3B:CA:CE:97:61:63:CD
	
	            X509v3 Basic Constraints: critical
	                CA:TRUE, pathlen:0
	            X509v3 Key Usage: critical
	                Certificate Sign, CRL Sign
	            X509v3 Subject Key Identifier: 
	                66:BC:95:BB:B5:DA:81:8B:68:00:FD:3F:42:BA:AC:0E:51:2A:C5:53
	            X509v3 CRL Distribution Points: 
	
	                Full Name:
	                  URI:https://pki.example.com/server.crl
	
	            Authority Information Access: 
	                CA Issuers - URI:https://pki.example.com/server.html
	
	Certificate is to be certified until Aug 21 11:27:24 2026 GMT (3652 days)
	Sign the certificate? [y/n]:y
	
	
	1 out of 1 certificate requests certified, commit? [y/n]y
	Write out database with 1 new entries
	Data Base Updated
	[master de441f1] Add signed certificate: O=Example, OU=Example PKI, CN=Example Server CA
	 7 files changed, 178 insertions(+), 4 deletions(-)
	 create mode 100644 certs/01.pem

#### Generate the CRL
	$ cd /safe/server-ca
	$ ./pki gencrl
	
	
	 -[*]- Generating certificate revokation list: ca/ca.crl
	
	Using configuration from openssl.conf
	Enter pass phrase for ca/ca.key:
	[master ae13bef] Update certificate revokation list: ca/ca.crl
	 4 files changed, 191 insertions(+), 1 deletion(-)
	 create mode 100644 ca/ca.crl
	 create mode 100644 ca/ca.crt
	 create mode 100644 db/crl.serial.old

### Issue a TLS server certificate

#### Configure the certificate request
	$ ./setup-server /safe/myserver.com
	
	The target directory has been configured from a reasonable template. Please
	edit the openssl.conf file before generating the CSR. The certificate request
	will include subjectAltName extensions even if you request a certificate for
	single domain.
	

#### Generate the CSR
	$ ./gencsr 4096 myserver.com www.myserver.com
	
	 -[*]- Generating the private key...
	
	Generating RSA private key, 4096 bit long modulus
	...++
	..................................................................++
	e is 65537 (0x10001)
	Enter pass phrase for myserver.com.key:
	Verifying - Enter pass phrase for myserver.com.key:
	
	 -[*]- Generating the certificate request...
	
	Enter pass phrase for myserver.com.key:

#### Sign the CSR using the intermediate CA
	$ cd /safe/server-ca
	$ ./pki sign /safe/myserver.com/myserver.com.csr /safe/myserver.com/myserver.com.crt
	
	 -[*]- Signing certificate request: ../myserver.com/myserver.com.csr
	
	Using configuration from openssl.conf
	Enter pass phrase for ca/ca.key:
	Check that the request matches the signature
	Signature ok
	Certificate Details:
	        Serial Number: 0 (0x0)
	        Validity
	            Not Before: Aug 21 11:36:36 2016 GMT
	            Not After : Aug 21 11:36:36 2017 GMT
	        Subject:
	            organizationName          = Example
	            organizationalUnitName    = Example Server Farm
	            commonName                = myserver.com
	        X509v3 extensions:
	            X509v3 Basic Constraints: 
	                CA:FALSE
	            X509v3 Key Usage: critical
	                Digital Signature, Key Encipherment
	            X509v3 Extended Key Usage: 
	                TLS Web Server Authentication, TLS Web Client Authentication
	            X509v3 Subject Key Identifier: 
	                67:93:4F:7F:EE:04:31:A8:16:BA:18:84:2A:49:7F:C5:82:F5:47:6A
	            X509v3 Authority Key Identifier: 
	                keyid:66:BC:95:BB:B5:DA:81:8B:68:00:FD:3F:42:BA:AC:0E:51:2A:C5:53
	
	            X509v3 CRL Distribution Points: 
	
	                Full Name:
	                  URI:https://pki.example.com/server.crl
	
	            Authority Information Access: 
	                CA Issuers - URI:https://pki.example.com/server.html
	
	            X509v3 Subject Alternative Name: 
	                DNS:myserver.com, DNS:www.myserver.com
	Certificate is to be certified until Aug 21 11:36:36 2017 GMT (365 days)
	Sign the certificate? [y/n]:y
	
	
	1 out of 1 certificate requests certified, commit? [y/n]y
	Write out database with 1 new entries
	Data Base Updated
	[master 1f71419] Add signed certificate: O=Example, OU=Example Server Farm, CN=myserver.com
	 8 files changed, 143 insertions(+), 3 deletions(-)
	 create mode 100644 certs/00.pem
	 create mode 100644 db/crt.serial.old
	 create mode 100644 db/index.attr.old
	 create mode 100644 db/index.old
